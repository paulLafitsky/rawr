//
//  GeoFenceViewController+Map.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import MapKit
import RealmSwift

extension GeoFenceViewController: MKMapViewDelegate {

	func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {

		if let annotation = annotation as? Artwork {
			let identifier = "pin"
			var view: MKPinAnnotationView

			if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
			as? MKPinAnnotationView {
				dequeuedView.annotation = annotation
				view = dequeuedView
			} else {
				view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
				view.canShowCallout = true
			}
			if let geofence = annotation.geofence {
				let detailView = GeofenceDetailView.getDetailView(geofence)
				detailView.delegate = self
				view.detailCalloutAccessoryView = detailView
				view.detailCalloutAccessoryView?.sizeToFit()
			}
			return view
		}
		return nil
	}

	func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
		let location = view.annotation as! Artwork
		let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
		location.mapItem().openInMapsWithLaunchOptions(launchOptions)
	}

	func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
		let circle = MKCircleRenderer(overlay: overlay)
		circle.strokeColor = UIColor.redColor()
		circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
		circle.lineWidth = 1
		return circle
	}
}