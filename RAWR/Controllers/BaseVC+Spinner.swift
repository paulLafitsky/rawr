//
//  BaseVC+Spinner.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/30/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import UIKit

extension BaseVC {

	func showSpinner() {
		hideSpinner()
		layoutSpinner()
		spinnerView?.start()
	}

	func hideSpinner() {
		spinnerView?.stop()
		spinnerView?.removeFromSuperview()
	}

	func getWindow() -> (UIWindow) {
		return UIApplication.sharedApplication().keyWindow!
	}

	// MARK: - Private

	private func layoutSpinner() {
		let window = getWindow()
		spinnerView = SpinnerView()
		spinnerView?.frame = window.frame
		spinnerView?.setupSpinner()
		window.addSubview(spinnerView!)
	}
}
