//
//  ViewController.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/19/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift
import LKAlertController

class GeoFenceViewController: BaseVC {

	let dataManager = GeofenceDataManager()
	var geofences: Results <Geofence>?
	var artworks = [Artwork]()
	var circles = [MKCircle]()
	let regionRadius: CLLocationDistance = 1000
	let radius = "200"

	var onenewArtWork: Artwork?

	@IBOutlet weak var mapView: MKMapView!

	override func viewDidLoad() {
		super.viewDidLoad()
		mapView.delegate = self
		showSpinner()
		dataManager.receiveGeofence({ [weak self](geofence) in
			self?.geofences = geofence
			self?.updateAnotation()
			self?.centerMap()
		}) { (errorText) in
		}
	}

	func updateAnotation() {
		mapView.removeAnnotations(artworks)
		mapView.removeOverlays(circles)
		artworks.removeAll()
		circles.removeAll()
		guard let geofences = geofences else {
			artworks = [Artwork]()
			return
		}

		artworks = geofences.map { (item) -> Artwork in
			return Artwork.artworkFromGeofence(item)!
		}

		circles = geofences.map { (item) -> Circle in
			return Circle.circkeFromGeofence(item)
		}
		mapView.addAnnotations(artworks)
		mapView.addOverlays(circles)
		self.hideSpinner()
	}

	// MARK: - User actions

	@IBAction func LongTapAction(sender: UILongPressGestureRecognizer) {
		if sender.state == .Ended {
			if let onenewArtWork = onenewArtWork {
				mapView.removeAnnotation(onenewArtWork)
			}
			let touchPoint = sender.locationInView(mapView)
			let newCoordinates = mapView.convertPoint(touchPoint, toCoordinateFromView: mapView)
			addGeofence(newCoordinates)
		}
	}

	func addGeofence(coordinate: CLLocationCoordinate2D) {
		var textField = UITextField()
		textField.placeholder = "Enter name"
		Alert(title: "Add new geofence").addTextField(&textField).addAction("Save", style: .Default, handler: { [weak self](action) in
			guard let name = textField.text else {
				return
			}
			let newOneGeofence = Geofence()
			newOneGeofence.name = name
			newOneGeofence.lon = String(format: "%f", coordinate.longitude)
			newOneGeofence.lat = String(format: "%f", coordinate.latitude)
			newOneGeofence.radius = (self?.radius)! // TEMP Radius
			self?.showSpinner()
			self?.dataManager.addGeofence(newOneGeofence, succes: { [weak self](geofence) in
				self?.geofences = geofence
				self?.updateAnotation()
				self?.hideSpinner()
				}, failure: { [weak self](errorText) in
				self?.hideSpinner()
			})

		}).show()
	}

	private func centerMap() {
		guard let firtPin = artworks.first else {
			return
		}
		let coordinateRegion = MKCoordinateRegionMakeWithDistance(firtPin.coordinate, regionRadius * 3.0, regionRadius * 3.0)
		mapView.setRegion(coordinateRegion, animated: true)
	}
}

// MARK: - GeofenceDetailViewDelegate

extension GeoFenceViewController: GeofenceDetailViewDelegate {
	func deleteAction(geofence: Geofence) {
		dataManager.deleteGeofence(geofence, succes: { [weak self](geofence) in
			self?.showSpinner()
			self?.geofences = self?.dataManager.recieveUpdatedData()
			self?.updateAnotation()
		}) { [weak self](errorText) in
			self?.hideSpinner()
		}
	}
}

