//
//  GeofenceDetailView.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/28/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

protocol GeofenceDetailViewDelegate: class {
	func deleteAction(geofence: Geofence)
}

class GeofenceDetailView: UIView {

	var geofence: Geofence?
	weak var delegate: GeofenceDetailViewDelegate?

	// MARK: Init
	class func getDetailView(geofence: Geofence) -> GeofenceDetailView {
		let view = UIView.loadFromNibNamed("GeofenceDetailView") as! GeofenceDetailView
		view.geofence = geofence
		return view
	}

	// MARK: Action
	@IBAction func removeAction(sender: AnyObject) {
		if let delegate = delegate {
			if let geofence = geofence {
				delegate.deleteAction(geofence)
			}
		}
	}
}
