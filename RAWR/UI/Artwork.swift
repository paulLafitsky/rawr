//
//  Artwork.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import MapKit
import AddressBook
import Contacts

class Artwork: NSObject, MKAnnotation {

	var title: String?
	var locationName: String
	var coordinate: CLLocationCoordinate2D
    var circle: MKCircle?
    var synch = false
    var geofence: Geofence?
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D, sync: Bool) {
		self.title = title
		self.locationName = locationName
		self.coordinate = coordinate
        synch = sync
		super.init()
	}
    
	var subtitle: String? {
		return locationName
	}

	func mapItem() -> MKMapItem {
		let addressDictionary = [String(CNPostalAddressStreetKey): self.subtitle!]
		let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDictionary)
		let mapItem = MKMapItem(placemark: placemark)
		mapItem.name = title
		return mapItem
	}
}
