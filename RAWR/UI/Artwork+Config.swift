//
//  Artwork+Config.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import MapKit
import AddressBook
import Contacts

extension Artwork {

	class func artworkFromGeofence(geofence: Geofence) -> Artwork? {
        let title = geofence.name
        let coordinate = CLLocationCoordinate2DMake(Double(geofence.lat)!, Double(geofence.lon)!)
        let item = Artwork(title: title, locationName: title, coordinate: coordinate, sync: true)
        item.geofence = geofence
		return item
	}
}