//
//  SpinnerView.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/30/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//


import UIKit
import DGActivityIndicatorView

class SpinnerView: UIView {
    
    var spinner: DGActivityIndicatorView?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        spinner?.center = center
    }
    
    // MARK: - Public
    
    func setupSpinner() {
        setupUI()
        spinner = DGActivityIndicatorView(type: .BallSpinFadeLoader, tintColor: UIColor.yellowColor(), size: 35)
        spinner?.center = center
        self.addSubview(spinner!)
    }
    
    func start() {
        guard let spinner = spinner else {
            return
        }
        spinner.startAnimating()
    }
    
    func stop() {
        guard let spinner = spinner else {
            return
        }
        spinner.stopAnimating()
    }
    
    // MARK: - Private
    
    private func setupUI () {
        self.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.4)
    }
}
