//
//  Circle.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/28/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import MapKit
import RealmSwift
class Circle: MKCircle {
 
}

extension Circle {
    
    class func circkeFromGeofence(geofence: Geofence) -> Circle {
        let coordinate = CLLocationCoordinate2DMake(Double(geofence.lat)!, Double(geofence.lon)!)
        let circle = Circle(centerCoordinate:coordinate, radius: Double(geofence.radius)! as CLLocationDistance)
        return circle
    }
}