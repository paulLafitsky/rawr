//
// Created by Paul Lafytskyi on 7/23/16.
// Copyright (c) 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import Alamofire

class GeofenceAPI:BaseAPI, GeofenceDataManagerProtocol {
    
    
    func receiveGeofence(succes: SuccessCallback, failure: FailureCallback) {
        let meta = GeofenceMeta()
        meta.method = Alamofire.Method.GET
        recieveGeofences(Geofence.self, mType: meta, succes: succes, failure: failure)
    }
    
    func deleteGeofence(geofence: Geofence, succes: SuccessCallback, failure: FailureCallback) {
        let meta = GeofenceMeta()
        meta.url = Endpoint.DeleteGeoFence.link(params: [geofence.id])
        meta.method = Alamofire.Method.DELETE
        meta.params = nil
        deleteGeofence(Geofence.self, mType: meta, succes: succes, failure: failure)
    }
    
    func createGeofence(geofence: Geofence, succes: SuccessCallback, failure: FailureCallback) {
        let meta = GeofenceMeta()
        meta.method = Alamofire.Method.POST
        meta.params = geofence.convertForRequest()
        creageGeofence(Geofence.self, mType: meta, succes: succes, failure: failure)
    }
}
