//
//  Endpoint.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/23/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation

enum Endpoint {
	case GeoFence
    case DeleteGeoFence

	func link(params params: [AnyObject]?) -> String {
		switch self {
		case .GeoFence:
			return geoFenceURL()
        case .DeleteGeoFence:
            return removeGeofenceURL(params)
		}
	}
    
    func link() -> String {
        return link(params: nil)
    }
    
    // MARK: - Private
    
    private func geoFenceURL() -> String {
        return baseURL() + "pets/1/geo_fence"
    }
    
    private func removeGeofenceURL(params: [AnyObject]?) -> String {
        guard let id = params?.first else {
            return baseURL()
        }
        return baseURL() + "geo_fence/\(id)"
    }
    
    private func baseURL() -> String {
        return Constants.API.baseURL
    }
}
