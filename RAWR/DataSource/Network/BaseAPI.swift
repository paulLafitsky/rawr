//
//  import Alamofire BaseAPI.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/23/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

typealias RequestCallback = (statusCode: Int, errorText: String?, data: NSData?) -> ()

class BaseAPI {

	// This one really ugly design. I shouldn't use alamofire object mapper. He is not flexible.

	func recieveGeofences<T: Object, M: MetaProtocol where T: Mappable> (type: T.Type, mType: M, succes: SuccessCallback, failure: FailureCallback) -> Void {
		guard let method = mType.method else {
			assert(false, "You should provide request method")
			return
		}
		generalRequest(method, url: mType.url, params: mType.params).responseArray { (response: Response<[T], NSError>) in
			switch response.result {
			case .Success(let items):
				autoreleasepool {
					do {
                        let realm = try! Realm()
						try realm.write {
							for item in items {
								realm.add(item, update: true)
							}
                            try! realm.commitWrite()
						}
					} catch let error as NSError {
						failure(errorText: error.localizedDescription)
					}
				}
				succes()
			case .Failure(let error):
				failure(errorText: error.localizedDescription)
			}
		}
	}

	func creageGeofence <T: Object, M: MetaProtocol where T: Mappable> (type: T.Type, mType: M, succes: SuccessCallback, failure: FailureCallback) -> Void {
		guard let method = mType.method else {
			assert(false, "You should provide request method")
			failure(errorText: "")
		}

		generalRequest(method, url: mType.url, params: mType.params).responseObject { (response: Response<T, NSError>) in
			switch response.result {
			case .Success(let item):
				autoreleasepool {
					do {
                        let realm = try! Realm()
						try realm.write {
							realm.add(item, update: true)
						}
					} catch let error as NSError {
						failure(errorText: error.localizedDescription)
                        return
					}
				}
				succes()
                return
			case .Failure(let error):
				failure(errorText: error.localizedDescription)
                return
			}
		}
	}

	func deleteGeofence <T: Object, M: MetaProtocol where T: Mappable> (type: T.Type, mType: M, succes: SuccessCallback, failure: FailureCallback) -> Void {
		guard let method = mType.method else {
			assert(false, "You should provide request method")
			failure(errorText: "")
            return
		}

		generalRequest(method, url: mType.url, params: mType.params).responseData { (response) in
			let statusCode: Int = response.response?.statusCode ?? 0
			if (self.isStatusCodeValid(statusCode)) {
                succes()
                return
			}
			failure(errorText: "some error")
		}
	}

	// MARK: - Private

	private func generalRequest (method: Alamofire.Method, url: String, params: [String: AnyObject]?) -> Request {
		let encoding = Alamofire.ParameterEncoding.JSON
		return Alamofire.request(method, url, parameters: params, encoding: encoding, headers: nil)
	}

	func isStatusCodeValid(statusCode: Int) -> Bool {
        var codes = [Int]()
        codes += 200...299
		return codes.contains(statusCode)
	}

}

