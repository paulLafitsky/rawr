//
//  GeoHelper.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import Alamofire

class GeofenceMeta : NSObject, MetaProtocol {
    var url: String = Endpoint.GeoFence.link()
    var method: Alamofire.Method?
    var params: [String : AnyObject]?
}