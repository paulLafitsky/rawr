//
//  Geofence.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Geofence: Object, Mappable {

	dynamic var id = 0
	dynamic var name = ""
	dynamic var lat = ""
	dynamic var lon = ""
	dynamic var pet_id = 0
	dynamic var radius = ""
	dynamic var created_at = ""
	dynamic var updated_at = ""

	override static func primaryKey() -> String? {
		return "id"
	}

	// Impl. of Mappable protocol

	required convenience init?(_ map: Map) {
		self.init()
	}

	func mapping(map: Map) {
		id <- map["id"]
		name <- map["name"]
		lat <- map["lat"]
		lon <- map["lon"]
		pet_id <- map["pet_id"]
		radius <- map["radius"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
	}

	func convertForRequest() -> [String: AnyObject] {
		var params = [String: AnyObject]()
		params["lat"] = lat
		params["lon"] = lon
		params["radius"] = radius
		params["name"] = name
		return params
	}

}

