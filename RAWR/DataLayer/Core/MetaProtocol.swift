//
//  APIProtocol.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import Alamofire

protocol MetaProtocol {
    var url: String {get set}
    var method: Alamofire.Method? {get set}
    var params: [String: AnyObject]? {get set}
}
