//
//  DataManager.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import RealmSwift

/// Datamanager used to provide mocks or real data. You can easy modify and provide new one handler with mocks.
/// Controller shouldn't know anything about *API Classes.

protocol GeofenceDataManagerProtocol {
	func receiveGeofence(succes: SuccessCallback, failure: FailureCallback)
	func createGeofence(geofence: Geofence, succes: SuccessCallback, failure: FailureCallback)
	func deleteGeofence(geofence: Geofence, succes: SuccessCallback, failure: FailureCallback)

}

class GeofenceDataManager {

	var currentHandler = GeofenceAPI()
	let realm = try! Realm()

	init () {
	}

	func receiveGeofence(succes: ReceiveGeofenceSuccessCallback, failure: FailureCallback) {
		currentHandler.receiveGeofence({
			let geofence = self.realm.objects(Geofence)
			succes(geofence: geofence)
			}, failure: failure)
	}

	func addGeofence(geofence: Geofence, succes: ReceiveGeofenceSuccessCallback, failure: FailureCallback) {
		currentHandler.createGeofence(geofence, succes: {
			let geofence = self.realm.objects(Geofence)
			succes(geofence: geofence)
			}, failure: failure)
	}

	func deleteGeofence(geofence: Geofence, succes: SuccessCallback, failure: FailureCallback) {
		currentHandler.deleteGeofence(geofence, succes: {
			self.realm.beginWrite()
			self.realm.delete(geofence)
            try! self.realm.commitWrite()
			succes()
		}) { (errorText) in
			failure(errorText: errorText)
		}
	}

	func recieveUpdatedData() -> Results<Geofence> {
		return self.realm.objects(Geofence)
	}
}