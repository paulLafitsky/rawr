//
//  DataManager.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import RealmSwift

typealias SuccessCallback = () -> ()
typealias ReceiveGeofenceSuccessCallback = (geofence: Results<Geofence>) -> ()
typealias FailureCallback = (errorText: String?) -> ()
