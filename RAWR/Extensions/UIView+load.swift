//
//  UIView+load.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/28/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import UIKit

extension UIView {
	class func loadFromNibNamed(nibNamed: String, bundle: NSBundle? = nil) -> UIView? {
		return UINib(
			nibName: nibNamed,
			bundle: bundle
		).instantiateWithOwner(nil, options: nil).first as? UIView
	}
}
