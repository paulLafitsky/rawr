//
//  Results+Array.swift
//  RAWR
//
//  Created by Paul Lafytskyi on 7/24/16.
//  Copyright © 2016 Paul Lafytskyi. All rights reserved.
//

import Foundation
import RealmSwift

extension Results {
    
    func toArray() -> [T] {
        return self.map{$0}
    }
}

extension RealmSwift.List {
    
    func toArray() -> [T] {
        return self.map{$0}
    }
}
